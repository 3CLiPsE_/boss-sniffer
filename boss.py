import socket
import json
import os
import sys
import datetime
import re
LISTEN_PORT = 1337
MSG_LENGTH = 1024
SETTINGS_FILE = "settings.dat"
REPORT_DIR = "report\\html\\"
TEMPLATE = "template.html"
UPDATE_REPORT = 10
DATA_CHUNK = 10
SERVER_ADDRESS = ('54.71.128.194', 8808)


def listen():
    """
    This function binds a UDP socket to a port
    :return: the socket
    :rtype: socket.socket
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', LISTEN_PORT))

    return sock


def get_agents():
    """
    This function reads the settings file and returns all agents
    :return: All agent names and addresses
    :rtype: dict
    """
    file_path = os.path.dirname(os.path.abspath(__file__)) + '\\' + SETTINGS_FILE
    if not os.path.isfile(file_path):
        print("\"settings.dat\" file wasn't found in project directory. Exiting...")
        sys.exit(1)     # exit the program
    else:
        with open(file_path) as settings:
            agents = {}
            settings = settings.read().split('\n')
            settings = settings[0].split(' = ')[1].split(',')
            for agent in settings:
                name, address = tuple(agent.split(':'))
                agents[address] = name

        return agents


def receive_msg(sock):
    """
    This function receives a message from an agent
    :param sock: socket to receive msg form
    :type sock: socket.socket
    :return: msg received / None if msg is not from agent
    :rtype: dict
    """
    msg_len, client_addr = sock.recvfrom(MSG_LENGTH)    # receive the length
    agents = get_agents()
    if client_addr[0] in agents:    # if msg not from one of agents ignore it
        try:
            msg, client_addr = sock.recvfrom(int(msg_len.decode()))
        except ValueError:  # make sure message gotten can be parsed
            return None
        msg = json.loads(msg)
        msg["agent"] = agents[client_addr[0]]
        print(f"Got {agents[client_addr[0]]}'s internet data.")
        return msg


def get_alerts(packets):
    """
    This function gets all alerts (blacklisted packets)
    :param packets: packets to check for alerts in
    :type packets: list
    :return: list of alerts
    :rtype: list
    """
    file_path = os.path.dirname(os.path.abspath(__file__)) + '\\' + SETTINGS_FILE
    if not os.path.isfile(file_path):
        print("\"settings.dat\" file wasn't found in project directory. Exiting...")
        sys.exit(1)     # exit the program
    else:
        with open(file_path) as settings:
            alerts = []
            blacklist = settings.read().split('\n')[1].split(" = ")[1].split(',')
            for site in blacklist:
                ip, domain = site.split(':')
                for packet in packets:
                    alert = (packet["agent"], ip)
                    if packet["serverIP"] == ip and alert not in alerts:    # append each agent once
                        alerts.append(alert)
        return alerts


def parse_data(packets):
    """
    This function parses packets as data for reports
    :param packets: packets to parse
    :type packets: list
    :return: parsed data
    :rtype: dict
    """
    data = {"agents_in": [[], []], "agents_out": [[], []], "countries": [[], []], "ips": [[], []], "apps": [[], []], "ports": [[], []], "alerts": []}
    for packet in packets:
        if packet["incoming"]:
            key = "agents_in"
        else:
            key = "agents_out"
        if packet["agent"] not in data[key][0]:
            data[key][0].append(packet["agent"])
            data[key][1].append(packet["packetSize"])
        else:
            data[key][1][data[key][0].index(packet["agent"])] += packet["packetSize"]   # if agent in list just add bytes

        if packet["serverCountry"] not in data["countries"][0]:
            data["countries"][0].append(packet["serverCountry"])
            data["countries"][1].append(packet["packetSize"])
        else:
            data["countries"][1][data["countries"][0].index(packet["serverCountry"])] += packet["packetSize"]

        if packet["serverIP"] not in data["ips"][0]:
            data["ips"][0].append(packet["serverIP"])
            data["ips"][1].append(packet["packetSize"])
        else:
            data["ips"][1][data["ips"][0].index(packet["serverIP"])] += packet["packetSize"]

        if packet["packetProgram"] not in data["apps"][0]:
            data["apps"][0].append(packet["packetProgram"])
            data["apps"][1].append(packet["packetSize"])
        else:
            data["apps"][1][data["apps"][0].index(packet["packetProgram"])] += packet["packetSize"]

        if packet["serverPort"] not in data["ports"][0]:
            data["ports"][0].append(packet["serverPort"])
            data["ports"][1].append(packet["packetSize"])
        else:
            data["ports"][1][data["ports"][0].index(packet["serverPort"])] += packet["packetSize"]

        data["alerts"] = get_alerts(packets)

    return data


def get_datetime():
    """
    This function gets the date and time and parses them into a string
    :return: datetime parsed as: "dd.mm.yyyy, hh:mm"
    :rtype: str
    """
    now = datetime.datetime.now()
    return now.strftime("%d.%m.%Y, %H:%M")


def create_report(internet_data):
    """
    This function creates an HTML report of data.
    :param internet_data: data to put into report
    :type internet_data: dict
    :return: report
    :rtype: str
    """
    template_path = os.path.dirname(os.path.abspath(__file__)) + '\\' + REPORT_DIR + TEMPLATE
    report_path = os.path.dirname(os.path.abspath(__file__)) + '\\' + REPORT_DIR + "report.html"
    try:
        with open(template_path) as template:
            report = template.read()    # read template
    except FileNotFoundError:
        print(f"Template file doesn't exist! Please download it and place into {template_path}")
        sys.exit(1)
    print("Creating report...")
    params = re.findall("%%.+?%%", report)  # parameters are written as "%%PARAM%%"
    internet_data = list(internet_data.items())
    report = report.replace(params[0], f"Last update: {get_datetime()}")
    for i, parameter in enumerate(params[1:-1]):
        i = int(i / 2)  # each parameter (except TIMESTAMP and ALERTS) are divided into 2: KEYS and VALUES
        report = report.replace(parameter, str(internet_data[i][1][0]) if "KEYS" in parameter else str(internet_data[i][1][1]))
    report = report.replace(params[-1], str(internet_data[-1][1]))

    with open(report_path, 'w') as report_file:
        report_file.write(report)
        print("Report created in \'{0}\'".format(report_path))
    return report


def upload_report(report, username):
    """
    This function uploads a report to the Magshimim server
    :param report: report to upload
    :type report: str
    :param username: username to upload with
    :type username: str
    :return: link to report if successfully uploaded, else None
    :rtype: str
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(SERVER_ADDRESS)
    print("Uploading report...")
    sock.sendall("400#USER={0}".format(username).encode())
    msg = sock.recv(MSG_LENGTH).decode()
    if "405" in msg:
        sock.sendall("700#SIZE={0},HTML={1}".format(len(report), report).encode())
        msg = sock.recv(MSG_LENGTH).decode()
        if "705" in msg:
            link = re.findall("[a-z]+_[a-z]+\.bossniffer\.com", msg)    # extract link from response
            sock.sendall("900#BYE".encode())
            sock.close()    # close socket before exit
            return link[0]
    sock.close()


def get_username():
    """
    This function gets a valid username as input.
    :return: valid username
    :rtype: str
    """
    while True:
        username = input("Enter username: ")
        matches = re.findall("^[a-z]+\.[a-z]+$", username)
        if len(matches) != 1:   # if less/more than one valid username input isn't valid
            print("Invalid input! ", end="")
        else:
            return username


def main():
    sock = listen()
    packets = []
    counter = 0
    try:
        username = get_username()
        while True:
            msg = None
            try:
                msg = receive_msg(sock)
            except socket.error:
                print("Error connecting to agent.")
            if msg is not None:     # if msg is None throw it away
                packets.append(msg)
                counter += 1
                if counter == UPDATE_REPORT:    # create report every 100 messages
                    report = create_report(parse_data(packets))
                    try:
                        link = upload_report(report, username)
                        print(f"Report uploaded at: {link}" if link is not None else "Upload failed.")
                    except socket.error:
                        print("Error connecting to Magshimim server. Please try again later.")
                    counter = 0
    except KeyboardInterrupt:
        print("Bye!")
    finally:
        sock.close()


if __name__ == "__main__":
    main()
