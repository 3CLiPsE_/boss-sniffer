from kamene.all import *
import requests
import json
import subprocess
import ctypes
import sys
import re
PACKET_COUNT = 10
BOSS_IP = '127.0.0.1'
BOSS_PORT = 1337
IP_COUNTRIES = {}   # global variable needed for processing


def is_admin():
    """
    This function checks if the program is running in Administrator mode.
    :return: True if running in admin mode, False else
    :rtype: bool
    """
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except Exception:
        return False


def get_internal_ip():
    """
    This function gets the internal IP of the host
    :return: internal IP of host
    :rtype: str
    """
    return socket.gethostbyname(socket.gethostname())


def get_external_ip():
    """
    This function gets the external IP of the host
    :return: external IP of host
    :rtype: str
    """
    data = requests.get("http://ip-api.com/json")
    data = json.loads(data.text)    # parse data
    return data["query"]


def filter_packet(packet):
    """
    This function checks if a packet is a TCP/UDP running over IP packet
    :param packet: packet to check
    :return: if packet matches filter or not
    :rtype: bool
    """
    return IP in packet and (TCP in packet or UDP in packet)


def print_addresses(packet):
    """
    This function prints the src and dst IP addresses of a packet
    :param packet: packet to print src and dst IP
    :return: None
    """
    print("%s -> %s" % (packet[IP].src, packet[IP].dst))


def get_country(ip_address):
    """
    This function gets the country of an IP address
    :param ip_address: address to check
    :type ip_address: str
    :return: country in which the IP is located
    :rtype: str
    """
    if ip_address in IP_COUNTRIES:
        country = IP_COUNTRIES[ip_address]
    else:
        ip_data = requests.get("http://ip-api.com/json/%s" % ip_address)
        ip_data = json.loads(ip_data.text)
        if ip_data["status"] == "fail" and ip_data["message"] == "private range":   # if IP is internal, it's in my subnet
            ip_data = requests.get("http://ip-api.com/json")
            ip_data = json.loads(ip_data.text)
        try:
            country = ip_data["country"]
        except KeyError:
            country = "Unknown"    # if cant get country putting "Unknown" (errors I cant resolve - listed here: http://ip-api.com/docs/unban)
        IP_COUNTRIES[ip_address] = country

    return country


def get_program(ip_address):
    netstat = subprocess.check_output("netstat -nb", shell=True).decode()
    pattern = re.compile("\d{1,3}[.]\d{1,3}\.\d{1,3}\.\d{1,3}")
    for match in pattern.finditer(netstat):     # for every IP address in the output
        if match.group() == ip_address:
            next_line = netstat[netstat.find('\r\n', match.span()[1]):netstat.find('\r\n', netstat.find('\r\n', match.span()[1]) + 2)]  # get the line of the program
            program = re.findall("\[.+?\]", next_line)  # get the program itself
            return program[0][1:-1] if len(program) == 1 else "Unknown"
    return "Unknown"


def process_packets(packets):
    """
    This function processes all packets gotten.
    :param packets: list of packets
    :type packets: list
    :return: msg containing all needed info on packets
    :rtype: list
    """
    msg = []
    print("Processing...")
    for packet in packets:
        data = dict()
        try:
            data["incoming"] = (packet[IP].dst == get_external_ip() or packet[IP].dst == get_internal_ip())     # if dst IP is my internal/external message in incoming
            data["serverIP"] = packet[IP].src if data["incoming"] else packet[IP].dst
            data["serverCountry"] = get_country(data["serverIP"])
            if UDP in packet:
                data["serverPort"] = packet[UDP].sport if data["incoming"] else packet[UDP].dport
            else:
                data["serverPort"] = packet[TCP].sport if data["incoming"] else packet[TCP].dport
            data["packetProgram"] = get_program(data["serverIP"])
            data["packetSize"] = len(packet)
            msg.append(data)
        except requests.exceptions.RequestException:
            print("Could not connect to internet. Please check your internet connection.")
            sys.exit(1)

    return msg


def send_data(msg):
    """
    This function sends data to the boss
    :param msg: data to send
    :type msg: list
    :return: none
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("Sending data...")
    for packet in msg:
        packet = json.dumps(packet)
        sock.sendto(str(len(packet)).encode(), (BOSS_IP, BOSS_PORT))   # send length
        sock.sendto(packet.encode(), (BOSS_IP, BOSS_PORT))
    sock.close()


def main():
    if is_admin():
        try:
            while True:
                msg = process_packets(sniff(count=PACKET_COUNT, lfilter=filter_packet, prn=print_addresses))
                send_data(msg)
        except KeyboardInterrupt:
            print("Bye!")
    else:
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 1)   # run as admin


if __name__ == "__main__":
    main()
